# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|
  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/trusty32"
  config.vm.box_check_update = false

  config.vm.network "forwarded_port", guest: 8069, host: 8069
  # config.vm.network "private_network", ip: "192.168.33.10"
  config.vm.network "private_network", type: "dhcp"

  # config.vm.network "public_network"

  # config.vm.synced_folder "../data", "/vagrant_data"

  config.vm.provider "virtualbox" do |vb|
    #vb.customize ["modifyvm", :id, "--nictype1", "Am79C973"]
    #vb.customize ["modifyvm", :id, "--nictype2", "Am79C973"]
    # Display the VirtualBox GUI when booting the machine
    vb.gui = false
  
    # Customize the amount of memory on the VM:
    vb.memory = "1024"
  end

  config.vm.provision "shell", inline: <<-SHELL
    wget http://download.gna.org/wkhtmltopdf/0.12/0.12.3/wkhtmltox-0.12.3_linux-generic-i386.tar.xz
    tar xf wkhtmltox-0.12.3_linux-generic-i386.tar.xz
    sudo mv wkhtmltox/bin/wkhtmltopdf /usr/bin/
    sudo mv wkhtmltox/bin/wkhtmltoimage /usr/bin/
    rm -r wkhtmltox
    rm -r wkhtmltox-0.12.3_linux-generic-i386.tar.xz

    echo "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
    sudo apt-get update
    sudo apt-get install -y python-dev libpq-dev libxml2-dev libxslt1-dev libevent-dev libsasl2-dev libldap2-dev libjpeg-dev libfreetype6-dev zlib1g-dev
    sudo ln -s /usr/lib/`uname -i`-linux-gnu/libfreetype.so /usr/lib/
    sudo ln -s /usr/lib/`uname -i`-linux-gnu/libjpeg.so /usr/lib/
    sudo ln -s /usr/lib/`uname -i`-linux-gnu/libz.so /usr/lib/
    sudo apt-get install -y python-pip git
    sudo apt-get install -y postgresql
    sudo su - postgres -c "createuser -s vagrant"
    sudo apt-get install -y nodejs
    sudo ln -s /usr/bin/nodejs /usr/bin/node
    sudo apt-get install -y npm
    sudo npm install -g less less-plugin-clean-css

    git clone https://www.github.com/odoo/odoo --depth 1 --branch 8.0 --single-branch /opt/odoo/
    sudo mkdir /etc/odoo/
    sudo mkdir /var/log/odoo/
    sudo chown vagrant:vagrant -R /opt/odoo
    sudo chown vagrant:vagrant -R /etc/odoo
    sudo chown vagrant:vagrant -R /var/log/odoo
    sudo pip install -r /opt/odoo/requirements.txt

    mkdir /vagrant/myaddons/
    echo -e "[options]\nadmin_passwd = admin\ndb_host = False\ndb_port = False\ndb_user = vagrant\ndb_password = False\naddons_path = /opt/odoo/openerp/addons,/opt/odoo/addons,/vagrant/myaddons\nlogfile=/var/log/odoo/odoo-server.log" | tee /etc/odoo/openerp-server.conf
    sudo apt-get install -y supervisor
    echo -e "[program:odoo]\ncommand=/opt/odoo/odoo.py --config /etc/odoo/openerp-server.conf\ndirectory=/opt/odoo\nuser=vagrant\nautostart=true\nautorestart=true\nstdout_logfile=/var/log/supervisor/odoo.stdout.log\nstderr_logfile=/var/log/supervisor/odoo.stderr.log" | sudo tee /etc/supervisor/conf.d/odoo.conf
    sudo supervisorctl reread
    sudo supervisorctl update
  SHELL
end
